<?php
    require_once $_SERVER['DOCUMENT_ROOT'] . '/helpers/Config.php'; //aponta para a raiz do servidor, não sendo necessario o ../
    require_once $_SERVER['DOCUMENT_ROOT'] . '/models/Plano.php'; //aponta para a raiz do servidor, não sendo necessario o ../
    
    checarLogado();

    function index(){
        $planos = listarPlanos();

        return $planos;
    }

    function visualizar($id){
        $plano = buscarPlano($id);
        return $plano;
    }

    function cadastrar(){
        $plano = [];

        if(!empty($_POST)){
            //echo "Clicou no botão";
            $plano = [
                'titulo' => $_POST['titulo'],
                'valor' => $_POST['valor'],
                'descricao' => $_POST['descricao']
            ];

            if(cadastrarPlano($plano)){
                header("Location:/admin/plano");
                exit;
            }

            //$plano['titulo'] = $_POST['titulo'];
        }

        return $plano;
    }

    function editar($id){
        $plano = buscarPlano($id);

        if(!empty($_POST)){
                $plano['titulo'] = $_POST['titulo'];
                $plano['valor'] = $_POST['valor'];
                $plano['descricao'] = $_POST['descricao'];

        if(editarPlano($plano)){
            header("Location:/admin/plano");
            exit;
        }
        }

        return $plano;
    }

    function deletar($id){
        if(deletarPlano($id)){
            header("Location:/admin/plano");
            exit;
        }
       
    }
?>
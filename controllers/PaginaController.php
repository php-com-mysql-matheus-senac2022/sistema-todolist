<?php
    require_once $_SERVER['DOCUMENT_ROOT'] . "/helpers/Config.php";
    require_once $_SERVER['DOCUMENT_ROOT'] . "/models/Pagina.php";

    function index(){
        $paginas = listarPaginas();
        return $paginas;
    }

    function visualizar($id){
        $pagina = buscarPagina($id);
        return $pagina;
    }

    function cadastrar(){

    }

    function editar($id){

    }

    function deletar($id){

    }
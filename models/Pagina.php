<?php
    require_once $_SERVER['DOCUMENT_ROOT'] . "/helpers/Config.php";
    require_once BANCO_DE_DADOS;

    function listarPaginas(){
        $db = conexao();
        $sql = "SELECT * FROM paginas";

        try{
            $stmt = $db->prepare($sql);
            $stmt->execute();
            return $stmt->fetchALL(PDO::FETCH_ASSOC);
        }catch(PDOException $e){
            die($e->getMessage());
            return false;
        }
    }

    function cadastrarPagina($pagina){

    }

    function buscarPagina($id){
        $db = conexao();

        $sql = "SELECT * FROM paginas WHERE id=:id";

        try{
            $stmt = $db->prepare($sql);
            $stmt->bindParam(":id", $id, PDO::PARAM_INT);
            $stmt->execute();
            return $stmt->fetch(PDO::FETCH_ASSOC);

        }catch(PDOException $e){
            die($e->getMessage());
            return false;

        }
    }

    function editarPagina($pagina, $id){

    }

    function deletarPagina($id){
        
    }